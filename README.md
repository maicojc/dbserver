# Teste DBServer

## Requisitos de ambiente necessários para compilar e rodar o software
Eclipse IDE for Enterprise Java Developers - Version: 2020-03
MySQL 8.0.20
Maven
Postmap
Java8

Script MySQL está no arquivo schema.sql na pasta /src/main/resources

## Instruções de como utilizar o sistema
Importe o projeto no Eclipse no padrão Maven.

Crie as tabelas com base no arquivo schema.sql, verifique se a Entity VotacaoView foi criada como view no MySQL.
Ajuste as configurações do arquivo application.properties para seu MySQL local ou remoto.


Acesse: http://localhost:8080 para acessar os dados exemplos, editar e incluir dados.

Os votos devem ser inluidos via Postmap para passar pelos testes.

Utilize Postmap, segue exemplo:
Metodo: Post

URL: http://localhost:8080/api/votacao

## Teste 1

Primeiro insert Json:

```Javascript
{
  "funcionario": {
    "id": 1
  },
  "refeitorio": {
    "id": 1
  },
  "semanas": {
      "id": 2
  }
}
```
Retorno: 

```Javascript
{"funcionario":1, "periodo":1, "refeitorio":1, "status":"Gravado com sucesso."}
```

## Teste 2

Mesmo funcionario escolhendo o mesmo refeitório em outro dia.

```Json
{
  "funcionario": {
    "id": 1
  },
  "refeitorio": {
    "id": 1
  },
  "semanas": {
      "id": 2
  }
}
```
Retorno: 

```bash
{"funcionario":1, "periodo":2, "refeitorio":1, "status":"Você já escolheu esse restaurante essa semana."}
```
## Teste 3

Mesmo funcionario escolhendo outro refeitório na mesma semana.

```Json
{
  "funcionario": {
    "id": 1
  },
  "refeitorio": {
    "id": 2
  },
  "semanas": {
      "id": 2
  }
}
```
Retorno: 

```bash
{"funcionario":1, "periodo":2, "refeitorio":2, "status":"Gravado com sucesso."}
```
## Teste 4

Mesmo funcionario escolhendo outro refeitório em outra semana.

```Json
{
  "funcionario": {
    "id": 1
  },
  "refeitorio": {
    "id": 1
  },
  "semanas": {
      "id": 8
  }
}
```
Retorno: 

```bash
{"funcionario":1, "periodo":8, "refeitorio":1, "status":"Gravado com sucesso."}
```

## O que vale destacar no código implementado?
As telas feitas no Thymeleaf fazem inclusão, edição, exclusão e consulta.
O voto pode ser incluido por tela mas somente implementei a regra no imput do Json, ou seja, ao incluir por tela o sistema aceita todos os imputs.
A API grava e retorna.


## O que poderia ser feito para melhorar o sistema?
Não coloquei mensagens de erro de exclusão de integragridade relacional e nem o teste solicitado no front, pois o foco era o back-end.
Tecnicamente esses dois ajustes deixariam o front ok.

## Algo a mais que você tenha a dizer:
Iniciei o projeto usando H2 mas quando fiz o get nos votos pela API retornou o looping por causa da relação bi-direcional, não encontrei muita sobre então conectei ao MySql,
montei uma view que retorna apenas os dados e deu certo.

## Duvidas sobre a API, consulte em:
[Swagger](http://localhost:8080/swagger-ui.html)

