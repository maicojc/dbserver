package com.dbserver.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dbserver.Model.Times;
import com.dbserver.Repository.TimesRepository;

@Controller
public class TimesController {

	@Autowired
	private TimesRepository repo;
	
	@GetMapping("/times/cadastro")
	private String formularioCadastroTimes(Model model) {
		
		model.addAttribute("time", new Times());

		return "times/time-cadastro";
	}

	@PostMapping("/times/salvar")
	public String salvarTimes(Times time, Model model) {
		
		repo.save(time);
		model.addAttribute("time", new Times());
		
		List<Times> times = repo.findAll();
		
		model.addAttribute("listaTimes", times);
		
		return "times/time-lista";
	}
	
	@GetMapping("/times/lista")
	public String times(Model model) {
		
		List<Times> times = repo.findAll();
		
		model.addAttribute("listaTimes", times);
		
		return "times/time-lista";
	}
	
	@GetMapping("/times/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		
		Optional<Times> time = repo.findById(id);
		
		model.addAttribute("time", time);
		
		return "times/time-cadastro";
	}
	
	@GetMapping("/times/excluir/{id}")
	public String excluir(@PathVariable Long id, Model model) {
		
		repo.deleteById(id);
		
		List<Times> times = repo.findAll();
		
		model.addAttribute("listaTimes", times);
		
		return "redirect:/times/lista";
	}
}
