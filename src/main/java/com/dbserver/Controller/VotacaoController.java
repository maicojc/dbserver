package com.dbserver.Controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dbserver.Model.Funcionarios;
import com.dbserver.Model.Refeitorios;
import com.dbserver.Model.Semanas;
import com.dbserver.Model.Votacao;
import com.dbserver.Repository.FuncionariosRepository;
import com.dbserver.Repository.RefeitoriosRepository;
import com.dbserver.Repository.SemanasRepository;
import com.dbserver.Repository.VotacaoRepository;

@Controller
public class VotacaoController {

	@Autowired
	private VotacaoRepository repo;
	
	@Autowired
	private FuncionariosRepository repoFunc;
	
	@Autowired
	private RefeitoriosRepository repoRefe;
	
	@Autowired
	private SemanasRepository repoSema;
	
	@GetMapping("/votacao/cadastro")
	private String formularioCadastroTimes(Model model) {
		
		model.addAttribute("votacao", new Votacao());

		return "votacao/votacao-cadastro";
	}

	@PostMapping("/votacao/salvar")
	public String salvarTimes(Votacao votacao, Model model) {
		
		repo.save(votacao);
		model.addAttribute("votacao", new Votacao());
		
		List<Votacao> votacaoL = repo.findAll();
		
		model.addAttribute("listaVotacao", votacaoL);
		
		return "redirect:/votacao/lista";
	}
	
	@GetMapping("/votacao/lista")
	public String times(Model model, Model totais) {
		
		List<Votacao> votacao = repo.listaVotos();
		
		model.addAttribute("listaVotacao", votacao);
		
		return "votacao/votacao-lista";
	}
	
	@GetMapping("/votacao/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		
		Optional<Votacao> votacao = repo.findById(id);
		
		model.addAttribute("votacao", votacao);
		
		return "votacao/votacao-cadastro";
	}
	
	@GetMapping("/votacao/excluir/{id}")
	public String excluir(@PathVariable Long id, Model model) {
		
		repo.deleteById(id);
		
		List<Votacao> votacao = repo.findAll();
		
		model.addAttribute("listaVotacao", votacao);
		
		return "redirect:/votacao/lista";
	}
	
	@ModelAttribute("votaFunc")
	public List<Funcionarios> listaFuncionarios(HttpServletRequest request){
		
		return repoFunc.findAll();
	}
	
	@ModelAttribute("votaRefe")
	public List<Refeitorios> listaRefeitorios(HttpServletRequest request){
		
		return repoRefe.findAll();
	}
	
	@ModelAttribute("votaSema")
	public List<Semanas> listaSemanas(HttpServletRequest request){
		
		return repoSema.findAll();
	}
}
