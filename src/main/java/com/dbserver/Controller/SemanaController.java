package com.dbserver.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dbserver.Model.Semanas;
import com.dbserver.Repository.SemanasRepository;

@Controller
public class SemanaController {

	@Autowired
	private SemanasRepository repo;
	
	@GetMapping("/semanas/cadastro")
	private String formularioCadastroSemanas(Model model) {
		
		model.addAttribute("semana", new Semanas());

		return "semanas/semana-cadastro";
	}

	@PostMapping("/semanas/salvar")
	public String salvarSemanas(Semanas semana, Model model) {
		
		repo.save(semana);
		model.addAttribute("semana", new Semanas());
		
		List<Semanas> semanas = repo.findAll();
		
		model.addAttribute("listaSemanas", semanas);
		
		return "semanas/semana-lista";
	}
	
	@GetMapping("/semanas/lista")
	public String semanas(Model model) {
		
		List<Semanas> semanas = repo.findAll();
		
		model.addAttribute("listaSemanas", semanas);
		
		return "semanas/semana-lista";
	}
	
	@GetMapping("/semanas/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		
		Optional<Semanas> semana = repo.findById(id);
		
		model.addAttribute("semana", semana);
		
		return "semanas/semana-cadastro";
	}
	
	@GetMapping("/semanas/excluir/{id}")
	public String excluir(@PathVariable Long id, Model model) {
		
		repo.deleteById(id);
		
		List<Semanas> semanas = repo.findAll();
		
		model.addAttribute("listaSemanas", semanas);
		
		return "redirect:/semanas/lista";
	}
}
