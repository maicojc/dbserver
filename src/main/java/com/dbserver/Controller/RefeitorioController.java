package com.dbserver.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dbserver.Model.Refeitorios;
import com.dbserver.Repository.RefeitoriosRepository;

@Controller
public class RefeitorioController {

	@Autowired
	private RefeitoriosRepository repo;
	
	@GetMapping("/refeitorios/cadastro")
	private String formularioCadastroRefeitorios(Model model) {
		
		model.addAttribute("refeitorio", new Refeitorios());

		return "refeitorios/refeitorio-cadastro";
	}

	@PostMapping("/refeitorios/salvar")
	public String salvarRefeitorios(Refeitorios refeitorio, Model model) {
		
		repo.save(refeitorio);
		model.addAttribute("refeitorio", new Refeitorios());
		
		List<Refeitorios> refeitorios = repo.findAll();
		
		model.addAttribute("listaRefeitorios", refeitorios);
		
		return "refeitorios/refeitorio-lista";
	}
	
	@GetMapping("/refeitorios/lista")
	public String refeitorios(Model model) {
		
		List<Refeitorios> refeitorios = repo.findAll();
		
		model.addAttribute("listaRefeitorios", refeitorios);
		
		return "refeitorios/refeitorio-lista";
	}
	
	@GetMapping("/refeitorios/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		
		Optional<Refeitorios> refeitorio = repo.findById(id);
		
		model.addAttribute("refeitorio", refeitorio);
		
		return "refeitorios/refeitorio-cadastro";
	}
	
	@GetMapping("/refeitorios/excluir/{id}")
	public String excluir(@PathVariable Long id, Model model) {
		
		repo.deleteById(id);
		
		List<Refeitorios> refeitorios = repo.findAll();
		
		model.addAttribute("listaRefeitorios", refeitorios);
		
		return "redirect:/refeitorios/lista";
	}
}
