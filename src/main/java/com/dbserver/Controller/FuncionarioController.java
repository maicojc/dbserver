package com.dbserver.Controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.dbserver.Model.Funcionarios;
import com.dbserver.Model.Times;
import com.dbserver.Repository.FuncionariosRepository;
import com.dbserver.Repository.TimesRepository;

@Controller
public class FuncionarioController {
	
	@Autowired
	private FuncionariosRepository repo;
	
	@Autowired
	private TimesRepository	repoTim;
	
	@GetMapping("/funcionarios/cadastro")
	private String formularioCadastroFuncionarios(Model model) {
		
		model.addAttribute("funcionario", new Funcionarios());

		return "funcionarios/funcionario-cadastro";
	}

	@PostMapping("/funcionarios/salvar")
	public String salvarFuncionarios(Funcionarios funcionario, Model model) {
		
		repo.save(funcionario);
		model.addAttribute("funcionario", new Funcionarios());
		
		List<Funcionarios> funcionarios = repo.findAll();
		
		model.addAttribute("listaFuncionarios", funcionarios);
		
		return "funcionarios/funcionario-lista";
	}
	
	@GetMapping("/funcionarios/lista")
	public String funcionarios(Model model) {
		
		List<Funcionarios> funcionarios = repo.findAll();
		
		model.addAttribute("listaFuncionarios", funcionarios);
		
		return "funcionarios/funcionario-lista";
	}
	
	@GetMapping("/funcionarios/editar/{id}")
	public String editar(@PathVariable Long id, Model model) {
		
		Optional<Funcionarios> funcionario = repo.findById(id);
		
		model.addAttribute("funcionario", funcionario);
		
		return "funcionarios/funcionario-cadastro";
	}
	
	@GetMapping("/funcionarios/excluir/{id}")
	public String excluir(@PathVariable Long id, Model model) {
		
		repo.deleteById(id);
		
		List<Funcionarios> funcionarios = repo.findAll();
		
		model.addAttribute("listaFuncionarios", funcionarios);
		
		return "redirect:/funcionarios/lista";
	}
	
	@ModelAttribute("timesFunc")
	public List<Times> listaTimes(HttpServletRequest request){
		
		return repoTim.findAll();
	}
}
