package com.dbserver.API;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dbserver.Repository.VotacaoRepository;
import com.dbserver.Repository.VotacaoViewRepository;
import com.dbserver.Model.Votacao;
import com.dbserver.Model.VotacaoView;

@RestController
@RequestMapping("/api/votacao")
public class votacaoAPI {
	
	@Autowired
	private VotacaoRepository repoVot;
	
	@Autowired
	private VotacaoViewRepository repoVotVw;
	
	@GetMapping()
	public ResponseEntity<List<VotacaoView>> votacao()
	{
		List<VotacaoView> votacao = repoVotVw.findAll();
		ResponseEntity<List<VotacaoView>> retorno = null;
		if(votacao.isEmpty()) {
			
			retorno = new ResponseEntity<>(HttpStatus.NO_CONTENT);
			
		} else {
			
			retorno = new ResponseEntity<>(votacao, HttpStatus.OK);
		}
		
		System.out.println(retorno);
		
		return retorno;
	}
	
	@PostMapping
	public ResponseEntity<?> salvar(@RequestBody Votacao votacao, BindingResult result) {
		
		ResponseEntity<?> retorno = null;
		
		System.out.println("Dados de entrada:" + votacao.toString());
		
		if(result.hasErrors()) {
			
			List<FieldError> erros = result.getFieldErrors();
			retorno = new ResponseEntity<>(erros, HttpStatus.BAD_REQUEST);
			
		} else {
			
			String retornoAPI = "";
		
			
			if(repoVot.testaVotoFuncionario(votacao.getFuncionario().getId(), votacao.getSemanas().getId()) > 0) {
				
				retornoAPI = "{\"funcionario\":" + votacao.getFuncionario().getId().toString() + 
								", \"periodo\":" + votacao.getSemanas().getId().toString()+
								", \"refeitorio\":" + votacao.getRefeitorio().getId().toString()+
							 ", \"status\":\"Você já votou hoje.\"}";
				
			} else {
				
				if(repoVot.testaRefeitorio(votacao.getFuncionario().getId(), 
										   votacao.getSemanas().getId(), 
						                   votacao.getRefeitorio().getId()) > 0) {
							 
					retornoAPI = "{\"funcionario\":" + votacao.getFuncionario().getId().toString() + 
								", \"periodo\":" + votacao.getSemanas().getId().toString()+
								", \"refeitorio\":" + votacao.getRefeitorio().getId().toString()+
								 ", \"status\":\"Você já escolheu esse restaurante essa semana.\"}";
				} else { 
					
					repoVot.save(votacao);
					
					retornoAPI = "{\"funcionario\":" + votacao.getFuncionario().getId().toString() + 
								", \"periodo\":" + votacao.getSemanas().getId().toString()+
								", \"refeitorio\":" + votacao.getRefeitorio().getId().toString()+
								", \"status\":\"Gravado com sucesso.\"}";
				}
			}
			
			retorno = new ResponseEntity<>(retornoAPI,HttpStatus.CREATED);
		}
		
		return retorno;
		
	}
	
}
