package com.dbserver.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbserver.Model.Semanas;

public interface SemanasRepository extends JpaRepository<Semanas, Long>{

}
