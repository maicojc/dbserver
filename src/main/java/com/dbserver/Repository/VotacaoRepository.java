package com.dbserver.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.dbserver.Model.Votacao;

public interface VotacaoRepository extends JpaRepository<Votacao, Long>{
	
	@Query("SELECT v, f FROM Votacao v Join Funcionarios f On f.id = v.funcionario  ")
	List<Votacao> listaVotos();
	
	@Query(value="select count(*) from Votacao t where t.votacao_funcionario=:funcionario and t.votacao_semanas=:semana", nativeQuery = true)
	int testaVotoFuncionario(@Param("funcionario") Long funcionario, @Param("semana") Long semanas);
	
	@Query(value="SELECT count(v.votacao_funcionario) FROM Votacao As v Join Semanas As s "
				+ "ON v.votacao_semanas = s.id "
				+ "where s.semana_Ano in (select semana_Ano from Semanas s where id=:semana) "
				+ "and v.votacao_funcionario=:funcionario and v.votacao_refeitorios=:refeitorio", nativeQuery = true)
	int testaRefeitorio(@Param("funcionario") Long funcionario, @Param("semana") Long semanas, @Param("refeitorio") Long refeitorio);

}
