package com.dbserver.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbserver.Model.Times;

public interface TimesRepository extends JpaRepository<Times, Long>{

}
