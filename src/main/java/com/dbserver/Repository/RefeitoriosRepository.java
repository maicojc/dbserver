package com.dbserver.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbserver.Model.Refeitorios;

public interface RefeitoriosRepository extends JpaRepository<Refeitorios, Long>{

}
