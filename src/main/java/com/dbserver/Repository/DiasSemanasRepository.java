package com.dbserver.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbserver.Model.DiasSemana;

public interface DiasSemanasRepository extends JpaRepository<DiasSemana, Long>{

}
