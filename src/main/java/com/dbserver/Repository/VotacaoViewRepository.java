package com.dbserver.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dbserver.Model.VotacaoView;

public interface VotacaoViewRepository extends JpaRepository<VotacaoView, Long>{

}
