package com.dbserver.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class VotacaoView {
	
	@Id
	private Long id;
	
	private String funcionario;
	private String restaurante;
	private String times;
	private String semana_ano;
	private String dia_semana;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}

	public String getRestaurante() {
		return restaurante;
	}

	public void setRestaurante(String restaurante) {
		this.restaurante = restaurante;
	}

	public String getTimes() {
		return times;
	}

	public void setTimes(String times) {
		this.times = times;
	}

	public String getSemana_ano() {
		return semana_ano;
	}

	public void setSemana_ano(String semana_ano) {
		this.semana_ano = semana_ano;
	}

	public String getDia_semana() {
		return dia_semana;
	}

	public void setDia_semana(String dia_semana) {
		this.dia_semana = dia_semana;
	}
}
