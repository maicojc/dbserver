package com.dbserver.Model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Semanas {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "diasSemanaId")
	private DiasSemana diasSemana;
	
	private String semanaAno;
	
	@OneToMany(mappedBy = "semanas")
	private List<Votacao> votacaoSemanas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DiasSemana getDiasSemana() {
		return diasSemana;
	}
	
	public String getDiasSemanaString() {
		return diasSemana.toString();
	}

	public void setDiasSemana(DiasSemana diasSemana) {
		this.diasSemana = diasSemana;
	}

	public String getSemanaAno() {
		return semanaAno;
	}

	public void setSemanaAno(String semanaAno) {
		this.semanaAno = semanaAno;
	}

	public List<Votacao> getVotacaoSemanas() {
		return votacaoSemanas;
	}

	public void setVotacaoSemanas(List<Votacao> votacaoSemanas) {
		this.votacaoSemanas = votacaoSemanas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((diasSemana == null) ? 0 : diasSemana.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((semanaAno == null) ? 0 : semanaAno.hashCode());
		result = prime * result + ((votacaoSemanas == null) ? 0 : votacaoSemanas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Semanas other = (Semanas) obj;
		if (diasSemana == null) {
			if (other.diasSemana != null)
				return false;
		} else if (!diasSemana.equals(other.diasSemana))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (semanaAno == null) {
			if (other.semanaAno != null)
				return false;
		} else if (!semanaAno.equals(other.semanaAno))
			return false;
		if (votacaoSemanas == null) {
			if (other.votacaoSemanas != null)
				return false;
		} else if (!votacaoSemanas.equals(other.votacaoSemanas))
			return false;
		return true;
	}
	
}
