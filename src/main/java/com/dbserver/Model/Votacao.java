package com.dbserver.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Votacao {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "votacaoFuncionario")
	private Funcionarios funcionario;
	
	@ManyToOne
	@JoinColumn(name = "votacaoRefeitorios")
    private Refeitorios refeitorio;
	
	
	@ManyToOne
	@JoinColumn(name = "votacaoSemanas")
    private Semanas semanas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Funcionarios getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionarios funcionario) {
		this.funcionario = funcionario;
	}

	public Refeitorios getRefeitorio() {
		return refeitorio;
	}

	public void setRefeitorio(Refeitorios refeitorio) {
		this.refeitorio = refeitorio;
	}
	
	public Semanas getSemanas() {
		return semanas;
	}

	public void setSemanas(Semanas semanas) {
		this.semanas = semanas;
	}

	@Override
	public String toString() {
		return "Votacao [id=" + id + ", funcionario=" + funcionario + ", refeitorio=" + refeitorio + ", semanas="
				+ semanas + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((funcionario == null) ? 0 : funcionario.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((refeitorio == null) ? 0 : refeitorio.hashCode());
		result = prime * result + ((semanas == null) ? 0 : semanas.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Votacao other = (Votacao) obj;
		if (funcionario == null) {
			if (other.funcionario != null)
				return false;
		} else if (!funcionario.equals(other.funcionario))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (refeitorio == null) {
			if (other.refeitorio != null)
				return false;
		} else if (!refeitorio.equals(other.refeitorio))
			return false;
		if (semanas == null) {
			if (other.semanas != null)
				return false;
		} else if (!semanas.equals(other.semanas))
			return false;
		return true;
	}
}
