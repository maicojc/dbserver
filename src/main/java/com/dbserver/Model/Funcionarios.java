package com.dbserver.Model;


import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Funcionarios {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String nome;
	
	@ManyToOne
	@JoinColumn(name = "id_time_fk")
	private Times timeId;
	
	
	@OneToMany(mappedBy = "funcionario")
	private List<Votacao> votacaoFuncionario;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Times getTimeId() {
		return timeId;
	}

	public void setTimeId(Times timeId) {
		this.timeId = timeId;
	}

	public List<Votacao> getVotacaoFuncionario() {
		return votacaoFuncionario;
	}

	public void setVotacaoFuncionario(List<Votacao> votacaoFuncionario) {
		this.votacaoFuncionario = votacaoFuncionario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((timeId == null) ? 0 : timeId.hashCode());
		result = prime * result + ((votacaoFuncionario == null) ? 0 : votacaoFuncionario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionarios other = (Funcionarios) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (timeId == null) {
			if (other.timeId != null)
				return false;
		} else if (!timeId.equals(other.timeId))
			return false;
		if (votacaoFuncionario == null) {
			if (other.votacaoFuncionario != null)
				return false;
		} else if (!votacaoFuncionario.equals(other.votacaoFuncionario))
			return false;
		return true;
	}
	
}
