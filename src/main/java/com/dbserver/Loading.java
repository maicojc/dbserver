package com.dbserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.dbserver.Model.DiasSemana;
import com.dbserver.Model.Funcionarios;
import com.dbserver.Model.Refeitorios;
import com.dbserver.Model.Semanas;
import com.dbserver.Model.Times;
import com.dbserver.Repository.DiasSemanasRepository;
import com.dbserver.Repository.FuncionariosRepository;
import com.dbserver.Repository.RefeitoriosRepository;
import com.dbserver.Repository.SemanasRepository;
import com.dbserver.Repository.TimesRepository;

@Component
public class Loading implements CommandLineRunner{

	@Autowired
	private TimesRepository repoTime;
	
	@Autowired
	private FuncionariosRepository repoFunc;
	
	@Autowired
	private RefeitoriosRepository repoRefe;
	
	@Autowired
	private SemanasRepository repoSem;
	
	@Autowired
	private DiasSemanasRepository repoDiasS;
	
	@Override
	public void run(String... args) throws Exception{
		
		Times times1 = new Times();
		
		times1.setId(1L);
		times1.setNome("DBAs");
		repoTime.save(times1);
		
		Times times2 = new Times();
		times2.setId(2L);
		times2.setNome("Analistas");
		repoTime.save(times2);
		
		Times times3 = new Times();
		times3.setId(3L);
		times3.setNome("Front-End");
		repoTime.save(times3);
		
		Times times4 = new Times();
		times4.setId(4L);
		times4.setNome("Back-End");
		repoTime.save(times4);
		
		Funcionarios funcionarios = new Funcionarios();
		
		funcionarios.setId(1L);
		funcionarios.setNome("Joao");
		funcionarios.setTimeId(times1);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(2L);
		funcionarios.setNome("Maria");
		funcionarios.setTimeId(times2);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(3L);
		funcionarios.setNome("Jose");
		funcionarios.setTimeId(times4);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(4L);
		funcionarios.setNome("Guilheme A");
		funcionarios.setTimeId(times2);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(5L);
		funcionarios.setNome("Guilheme B");
		funcionarios.setTimeId(times1);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(6L);
		funcionarios.setNome("Paula");
		funcionarios.setTimeId(times3);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(7L);
		funcionarios.setNome("Alice");
		funcionarios.setTimeId(times4);
		repoFunc.save(funcionarios);
		
		funcionarios.setId(8L);
		funcionarios.setNome("Augusto");
		funcionarios.setTimeId(times4);
		repoFunc.save(funcionarios);
		
		Refeitorios refeitorios = new Refeitorios();
		
		refeitorios.setId(1L);
		refeitorios.setNome("Restaurante da Joana");
		repoRefe.save(refeitorios);
		
		refeitorios.setId(2L);
		refeitorios.setNome("Restaurante do Ari");
		repoRefe.save(refeitorios);
		
		refeitorios.setId(3L);
		refeitorios.setNome("Restaurante da Maria");
		repoRefe.save(refeitorios);
		
		DiasSemana diasSemana1 = new DiasSemana();
		
		diasSemana1.setDiaSemana("DOMINGO");
		repoDiasS.save(diasSemana1);
		
		DiasSemana diasSemana2 = new DiasSemana();
		
		diasSemana2.setDiaSemana("SEGUNDA");
		repoDiasS.save(diasSemana2);
		
		DiasSemana diasSemana3 = new DiasSemana();
		
		diasSemana3.setDiaSemana("TERCA");
		repoDiasS.save(diasSemana3);
		
		DiasSemana diasSemana4 = new DiasSemana();
		
		diasSemana4.setDiaSemana("QUARTA");
		repoDiasS.save(diasSemana4);
		
		DiasSemana diasSemana5 = new DiasSemana();
		
		diasSemana5.setDiaSemana("QUINTA");
		repoDiasS.save(diasSemana5);
		
		DiasSemana diasSemana6 = new DiasSemana();
		
		diasSemana6.setDiaSemana("SEXTA");
		repoDiasS.save(diasSemana6);
		
		DiasSemana diasSemana7 = new DiasSemana();
		
		diasSemana7.setDiaSemana("SABADO");
		repoDiasS.save(diasSemana7);
		
		Semanas semanas = new Semanas();
		
		semanas.setId(1L);
		semanas.setDiasSemana(diasSemana1);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(2L);
		semanas.setDiasSemana(diasSemana2);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(3L);
		semanas.setDiasSemana(diasSemana3);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(4L);
		semanas.setDiasSemana(diasSemana4);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(5L);
		semanas.setDiasSemana(diasSemana5);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(6L);
		semanas.setDiasSemana(diasSemana6);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(7L);
		semanas.setDiasSemana(diasSemana7);
		semanas.setSemanaAno("SEMANA1");
		repoSem.save(semanas);
		
		semanas.setId(8L);
		semanas.setDiasSemana(diasSemana1);
		semanas.setSemanaAno("SEMANA2");
		repoSem.save(semanas);
		
		semanas.setId(9L);
		semanas.setDiasSemana(diasSemana2);
		semanas.setSemanaAno("SEMANA2");
		repoSem.save(semanas);
		
		semanas.setId(10L);
		semanas.setDiasSemana(diasSemana3);
		semanas.setSemanaAno("SEMANA2");
		repoSem.save(semanas);
	}
}
