DROP TABLE IF EXISTS "times";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "times" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
) ;

DROP TABLE IF EXISTS "dias_semana";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "dias_semana" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "dia_semana" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
);

DROP TABLE IF EXISTS "funcionarios";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "funcionarios" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) DEFAULT NULL,
  "id_time_fk" bigint DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "FKoqsivqxi4koev3ewug6hsru2q" ("id_time_fk"),
  CONSTRAINT "FKoqsivqxi4koev3ewug6hsru2q" FOREIGN KEY ("id_time_fk") REFERENCES "times" ("id")
) ;

DROP TABLE IF EXISTS "refeitorios";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "refeitorios" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "nome" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
) ;

DROP TABLE IF EXISTS "semanas";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "semanas" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "semana_ano" varchar(255) DEFAULT NULL,
  "dias_semana_id" bigint DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "FKfgm3xy68h10xprn0dumqwxbd9" ("dias_semana_id"),
  CONSTRAINT "FKfgm3xy68h10xprn0dumqwxbd9" FOREIGN KEY ("dias_semana_id") REFERENCES "dias_semana" ("id")
) ;

DROP TABLE IF EXISTS "votacao_registro";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "votacao_registro" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "funcionario" varchar(255) DEFAULT NULL,
  "refeitorio" varchar(255) DEFAULT NULL,
  "semanas" varchar(255) DEFAULT NULL,
  PRIMARY KEY ("id")
) ;

DROP TABLE IF EXISTS "votacao";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE "votacao" (
  "id" bigint NOT NULL AUTO_INCREMENT,
  "votacao_funcionario" bigint DEFAULT NULL,
  "votacao_refeitorios" bigint DEFAULT NULL,
  "votacao_semanas" bigint DEFAULT NULL,
  PRIMARY KEY ("id"),
  KEY "FKfke8jt887i3ufrv9uvu43qjhn" ("votacao_funcionario"),
  KEY "FKj94vsa2dfm7ik31w8r51ld2f2" ("votacao_refeitorios"),
  KEY "FKhk697k9br9qlty35393dwwwcx" ("votacao_semanas"),
  CONSTRAINT "FKfke8jt887i3ufrv9uvu43qjhn" FOREIGN KEY ("votacao_funcionario") REFERENCES "funcionarios" ("id"),
  CONSTRAINT "FKhk697k9br9qlty35393dwwwcx" FOREIGN KEY ("votacao_semanas") REFERENCES "semanas" ("id"),
  CONSTRAINT "FKj94vsa2dfm7ik31w8r51ld2f2" FOREIGN KEY ("votacao_refeitorios") REFERENCES "refeitorios" ("id")
) ;

CREATE OR REPLACE
VIEW "votacao_view" AS
    SELECT 
        "v"."id" AS "id",
        "f"."nome" AS "funcionario",
        "r"."nome" AS "restaurante",
        "t"."nome" AS "times",
        "s"."semana_ano" AS "semana_ano",
        "d"."dia_semana" AS "dia_semana"
    FROM
        ((((("votacao" "v"
        JOIN "funcionarios" "f")
        JOIN "refeitorios" "r")
        JOIN "times" "t")
        JOIN "semanas" "s")
        JOIN "dias_semana" "d")
    WHERE
        (("f"."id" = "v"."votacao_funcionario")
            AND ("r"."id" = "v"."votacao_refeitorios")
            AND ("t"."id" = "f"."id_time_fk")
            AND ("s"."id" = "v"."votacao_semanas")
            AND ("d"."id" = "s"."dias_semana_id"))